
package com.example;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main class of application that takes String of integers
 * @author Anar
 */


public class Main {
    
    private static final Logger log = LogManager.getLogger(SortingApp.class);
    public static void main(String[] args) {
        log.info("Sorting process starts");
        
        System.out.println("Please write ten natural numbers and use space between them");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        final SortingApp sortingApp = new SortingApp();
        String output = sortingApp.sort(s);
        System.out.println(output);
    }
}
