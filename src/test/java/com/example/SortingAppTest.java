
package com.example;

import org.junit.Test;

/**
 *
 * @author Anar
 */
public class SortingAppTest {
    
    private SortingApp sort = new SortingApp();
    
    @Test(expected = NullPointerException.class)
    public void testNullException() {
        String args = null;
        sort.sort(args);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testNoIntegerAsArgException() {
        String args = "1 2 three four";
        sort.sort(args);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgException() {
        String args = "";
        sort.sort(args);
    }
   

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenElementsException() {
        String args = "150 -150 250 -250 350 -350 450 -450 550 -550 650";
        sort.sort(args);
    }

}
